<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ProfileController extends Controller
{
    //
    public function profile(){
        $id=Auth::user()->id;
        return view('auth.profile');
    }
    public function home()
    {
        return view('home');
    }

}
